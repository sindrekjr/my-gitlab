# My Gitlab
A simple application which will render a card with Gitlab user information, derived from the standard learn-react template. The app is made as part of a study course task.

## Configuration
The application will fetch user information depending on whose private access token is supplied as an environment variable when the application is run. Environment variables may be set in a local .env file or through any other process configuration. React will automatically pick up any .env variables prefixed with `REACT_APP_`. 

## Project structure
The application consists of two simple components. 
* App
* Card

App is the main component which renders the rest of the page, after being called by the entry point index.js. Card manages the rendered user information "card". 
