import React, { Component } from 'react';
import './App.css';
import Card from './Card.js';

import { withRouter } from 'react-router-dom';


class App extends Component {
  state = { 
    loading: true,
    user: null
  };

  componentDidMount() {
    this.asyncLoad().then(response => response.json()).then(json => {
      this.setState({ loading: false, user: json });
    })
  }

  async asyncLoad() {
    window.location.hash = '';
    const { location } = this.props;

    if(!location.hash) {
      const params = new URLSearchParams({
        redirect_uri: window.location.href.slice(0, -1),
        client_id: process.env.REACT_APP_CLIENT_TOKEN,
        response_type: 'token'
      });
  
      window.location = `https://gitlab.com/oauth/authorize?${params.toString()}`;
    }

    const token = new URLSearchParams(location.hash.substring(1)).get('access_token');
    if(token) {
      return fetch(
        'https://gitlab.com/api/v4/user',
        {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }
      );
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <svg className="App-logo" width="140" height="140" viewBox="0 0 36 36">
            <path className="tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"></path>
            <path className="tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"></path>
            <path className="tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"></path>
            <path className="tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"></path>
            <path className="tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"></path>
            <path className="tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"></path>
            <path className="tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"></path>
          </svg>
        </header>
        <main>
          {this.renderMain()}
        </main>
      </div>
    );
  }

  renderMain() {
    if(this.state.loading || !this.state.user) {
      return <h1 className="loading">Loading</h1>;
    } else {
      return <Card user={this.state.user} />;
    }
  }
}

export default withRouter(App);
