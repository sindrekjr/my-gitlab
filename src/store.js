/**
 * Dependencies
 * @ignore
 */
import { createStore } from 'redux';

/**
 * Module dependencies
 * @ignore
 */
import { accessTokenReducer } from './reducer'

const store = createStore(accessTokenReducer);

/**
 * Exports
 * @ignore
 */
export default store;
