import React from 'react';
import './Card.css';

class Card extends React.Component {
  render() {
    return (
      <div className="Card">
        <section>
          <img className="user-avatar" src={this.props.user.avatar_url} alt="avatar" />
        </section>
        <section>
          <a className="user-name" href={this.props.user.web_url}>{this.props.user.name}</a>
          <a className="user-username" href={this.props.user.web_url}>{'@' + this.props.user.username}</a>
        </section>
      </div>
    );
  }
}

export default Card;