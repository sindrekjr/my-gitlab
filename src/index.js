import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom'

/**
 * Module Dependencies
 * @ignore
 */
import store from './store';
import * as serviceWorker from './serviceWorker';
import './index.css';
import App from './App';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>, document.getElementById('root'));

/*fetch(
  'https://gitlab.com/api/v4/user',
  {
    headers: {
      'Access-Control-Allow-Origin': '',
      'Content-Type': 'application/json',
      'Private-Token': process.env.REACT_APP_API_TOKEN
    }
  }
).then(response => response.json()).then(json => {
  ReactDOM.render(<App user={json} />, document.getElementById('root'));
});*/


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
